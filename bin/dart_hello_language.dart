import 'package:dart_hello_language/dart_hello_language.dart'
    as dart_hello_language;

void main(List<String> arguments) {
  //print('Hello world: ${dart_hello_language.calculate()}!');
  print(dart_hello_language.testeSoma(5, 3)); // Imprime 8
  print(dart_hello_language.testeMedia([2.0, 3.0, 4.0])); // Imprime 3.0
  //dart_hello_language.testeString();
  //dart_hello_language.testeEstruturasCondicionais(36, 'casado');
  //dart_hello_language.testeEstruturasRepeticao();
  //dart_hello_language.testeListasMap();
  //leituraIO();
}
