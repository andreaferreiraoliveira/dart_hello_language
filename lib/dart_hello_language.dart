import 'dart:io';

int calculate() {
  return 6 * 7;
}

void testeString() {
  int idade = 25;
  double altura = 1.75;
  String nome = 'João';
  bool ativo = true;

  print('Idade:  $idade');
  print('Idade:  $altura');
  print('Idade:  $nome');
  print('Idade:  $ativo');
}

void testeEstruturasCondicionais(int idade, String estadoCivil) {
  if (idade >= 18) {
    print('É maior de idade.');
  } else {
    print('É menor de idade.');
  }

  //String estadoCivil = 'casado';
  switch (estadoCivil) {
    case 'solteiro':
      print('Solteiro(a)');
      break;
    case 'casado':
      print('Casado(a)');
      break;
    default:
      print('Estado civil desconhecido');
  }
}

void testeEstruturasRepeticao() {
  for (int i = 0; i < 5; i++) {
    print('Número: $i');
  }

  int contador = 0;
  while (contador < 3) {
    print('Contador: $contador');
    contador++;
  }

  do {
    print('Isso será executado pelo menos uma vez.');
  } while (false);
}

void testeListasMap() {
  List<String> frutas = ['maçã', 'banana', 'laranja'];
  print(frutas[0]); // Imprime "maçã"

  Map<String, int> idades = {
    'João': 30,
    'Maria': 25,
    'Carlos': 40,
  };
  print(idades['João']); // Imprime 30
}

void testeLeituraIO() {
  stdout.write('Digite seu nome: ');
  String nome = stdin.readLineSync()!; // Lê a linha do console

  stdout.write('Digite sua idade: ');
  int idade =
      int.parse(stdin.readLineSync()!); // Lê a linha e converte para int

  print('Olá, $nome! Você tem $idade anos.');
}

int testeSoma(int a, int b) {
  return a + b;
}

double testeMedia(List<double> valores) {
  double soma = 0;
  for (var valor in valores) {
    soma += valor;
  }
  return soma / valores.length;
}
